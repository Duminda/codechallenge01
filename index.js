var express = require('express'),
	jade = require('jade'),
	path = require('path'),
	twitter = require('twit'),
	routes = require('./routes');

	app = express();

	app.use(express.static(path.join(__dirname, 'public')));
	app.use(express.cookieParser());
	app.use(express.session({secret: "twitter secret"}));

	app.use(express.urlencoded())
	app.use(express.json())

	app.configure(function(){
		app.set('PORT',3000);
	})

	routes(app);

	app.listen(3000,function(err){
		if(err)	throw err;

		console.log("Server now listining to port 3000 on localhost");
	});

