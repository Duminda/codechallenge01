/*
 *
 * discrtiption: Twitter search and outputs most ten frequent keywords for given tweet count 
 * author: Duminda Wanninayake
 * date: 16-Jan-2014
 *
 */

var twitter = require('twit');
var properties = require('../config');

//how many tweets to be processed
var COUNT = 100;

var T = new twitter({
	consumer_key: properties.CONSMR_KEY,
	consumer_secret: properties.CONSMR_SCRT,
	access_token: properties.ACCS_TKN,
	access_token_secret: properties.ACCS_TKN_SCRT
});


module.exports = function(app){

	//count execution time for the process in async way
	function execution_time(time, callback){

		var time_now = new Date().getTime();

		callback(time_now - time);
	}

	//split the reply and array it (with word cleanup)
	function split_and_to_array(reply, txt_in, txt_out, callback){

		var i=0;j=0,k=0;

		if(typeof reply != 'undefined'){

			for(i=0; i<reply.length; i++)
				txt_in += reply[i].text;
			

			for(k=0; k<reply.length; k++)
				txt_out = txt_in.split(" ");
			
		
			for(j=0; j<txt_out.length; j++){
				txt_out[j] = txt_out[j].toLowerCase().replace(/[^\w\s]/gi, '');
				if(txt_out[j] == '')
					txt_out.splice(j, 1);
			}

			txt_out.sort();
		}
		callback(txt_out);
	}

	var common_words = ["the","and","or","of","a","an","on","in","to","be","i","will","with","is","are","am","for","have","has","was","it","at","by","this","that"];

    //skipping unwanted words from array
	function skip_words(txt_out, callback){
		for(var j=0;j<txt_out.length;j++){
			for(var i=0;i<common_words.length;i++){
				if(txt_out.indexOf(common_words[i]) != -1)
					txt_out.splice(txt_out.indexOf(common_words[i]), 1);
				
			}
		}
		callback(txt_out);
	}

    //arange the array as a key value pair 
	function result_to_json_array(input_text, output_txt_array, callback){

		var cnt=0,x=0;

		for(x=0; x<input_text.length; x++){
		
			if(input_text[x+1] === input_text[x]){

				cnt++;

			}else{
				
				if(cnt > 0){

					var obj = {};
					obj.text = input_text[x];
					obj.count = cnt + 1;

					output_txt_array.push(obj);

				}

				cnt = 0;
			}	
		}
		callback(output_txt_array);
	}
	
	app.get('/', function(req, res){
		res.render('home.jade');
	})

	app.post('/', function(req, res){

		
	
		T.get('statuses/user_timeline', {screen_name: req.body.name, count: req.body.count}, function(err, reply){

			var start_time = new Date().getTime();
			
			var textInput = "",textOutput = "",result_array_output = [],name = req.body.name;	

			textOutput = split_and_to_array(reply, textInput, textOutput,function(textOutput){
				//skip_words(textOutput, function(textOutput){
					result_to_json_array(textOutput, result_array_output,function(result_array_output){

						result_array_output = result_array_output.sort(function(a,b) { return parseFloat(b.count) - parseFloat(a.count) } );
						result_array_output = result_array_output.slice(0,10);

						execution_time(start_time, function(time_taken){
							var time = time_taken/1000;
							res.render('home.jade', {data: result_array_output, name: name, time: time});
						})
					})
				//})
			})

		})
	})
};